"use strict"
var themeIcon = document.getElementById("theme_icon");

themeIcon.onclick = function(){
       document.body.classList.toggle("dark-theme");
       if(document.body.classList.contains("dark-theme")){
              themeIcon.src = "img hw4/sun.png";
       }else{
              themeIcon.src = "img hw4/moon.png";
       }
}
